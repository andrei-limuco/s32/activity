const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");

//Route for checking email exists
router.post("/checkEmail", (req, res) => {
    userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

//Route for User Registration
router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});


//Route for User Authentication
router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

//Route for User Details
//The "auth.verify" will acts as a middleware to ensure that the user is logged in before they can get the details
router.get("/details", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);
    console.log(userData);

    userController.getProfile({ userId: userData.id }).then(resultFromController => res.send(resultFromController));
});


// router.post("/enroll", auth.verify, (req, res) => {

// 	let data = {
// 		courseId: req.body.courseId
// 	}

// 	userController.enroll(data).then(resultFromController => res.send(resultFromController));
// });


//Activity
router.post("/enroll", auth.verify, (req, res) => {

    let data = {
        courseId: req.body.courseId,
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        userId: auth.decode(req.headers.authorization).id
    }

    if (data.isAdmin) {
        res.send("Unable to enroll")
    } else {
        userController.enroll(data).then(resultFromController => res.send(resultFromController));
    }
})

module.exports = router;