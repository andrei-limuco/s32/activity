const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require("../auth");

//Route for create a course
/*
router.post("/", (req, res) => {

        courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
});

*/
//Session 34 - Activity - Solution 1

router.post('/', auth.verify, (req, res) => {
        const userData = auth.decode(req.headers.authorization)
        
        if(userData.isAdmin) {
                courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
        } else {
                res.send("Not Authorized")
        }
})


//Activity Solution 2
/*router.post("/", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    courseController.addCourse(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController))
})*/


//Route for retrieving all the courses
router.get("/all", auth.verify, (req, res) => {

        courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
})

//Mini-Activity
/*
        1. Create a route and controller for retrieving all the courses from the database with the property of "isActive" to true. No need to logged in.

        getAllActive
        "/"

*/

//Route for retrieving all the Active courses
router.get("/", (req, res) => {

        courseController.getAllActive().then(resultFromController => res.send(resultFromController))
});

//Route for retrieving a specific course
//Url: localhost:4000/courses/622515452125423415
router.get("/:courseId", (req, res) => {
        console.log(req.params)

        courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));

})

//Route for updating a course
router.put("/:courseId", auth.verify, (req, res) => {

        const data = {
                courseId: req.params.courseId,
                isAdmin: auth.decode(req.headers.authorization).isAdmin,
                updatedCourse: req.body
        }

        courseController.updateCourse(data).then(resultFromController => res.send(resultFromController));
})


//Activity
//Archiving a course
router.put("/:courseId/archive", auth.verify, (req, res) => {

        const data = {
                courseId: req.params.courseId,
                isAdmin: auth.decode(req.headers.authorization).isAdmin
        }

        courseController.archiveCourse(data).then(resultFromController => res.send(resultFromController));
})





module.exports = router;
