const Course = require("../models/Course");

//Create a course - ADMIN Only
//Session 34 - Activity - Solution 1

module.exports.addCourse = (reqBody) => {

	let newCourse = new Course ({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price

	})

	return newCourse.save().then((course, error) => {

		if(error) {
			return false
		} else {
			return true
		}

	})
}


//Retrieve all Courses

module.exports.getAllCourses = () => {

	return Course.find({}).then(result => {
		return result
	})
}

//Retrieve All Active Courses
module.exports.getAllActive = () => {

	return Course.find({isActive: true}).then(result => {
		return result
	})
}


//Retrieve a specific course
module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
} 


//Updating a course
module.exports.updateCourse = (data) => {
	console.log(data)

	return Course.findById(data.courseId).then((result, error) => {

		console.log(result);

		if(data.isAdmin){
			result.name = data.updatedCourse.name
			result.description = data.updatedCourse.description
			result.price = data.updatedCourse.price

			console.log(result)

			return result.save().then((updatedCourse, error) => {

				if(error){
					return false
				} else {
					return updatedCourse
				}
			})

		} else {
			return "Not Admin"
		}
	})
}



module.exports.archiveCourse = (data) => {
	console.log(data)

	return Course.findById(data.courseId).then((result, error) => {

		console.log(result);

		if(data.isAdmin){
			result.isActive = false

			console.log(result)

			return result.save().then((archivedCourse, error) => {

				if(error){
					return false
				} else {
					return true
				}
			})

		} else {
			return "Not Admin"
		}
	})
}