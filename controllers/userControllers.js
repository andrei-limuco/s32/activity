const User = require("../models/User");
const Course = require("../models/Course")
const bcrypt = require("bcrypt");
const auth = require("../auth");

//Check if Email exists
module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then( result => {

		if(result.length > 0) {
			return true
		} else {
			return false
		}
	})
}


//User Registration 
module.exports.registerUser = (reqBody) => {

	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		//10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		//hashSync(<dataToBeHashed),<salt>)
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {

		if (error) {
			return false
		} else {
			return true
		}
	})
}


//User Authentication 
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null){
			return false
		} else {

			//compareSync(dataToBeCompared, ecryptedData)
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return { access: auth.createAccessToken(result)}
			} else {
				return false;
			}
		}
	})
}


//Retrieve User Details
module.exports.getProfile = (data) => {

	return User.findById(data.userId).then((result, error) => {
		if (error) {
			return false
		} else {
			result.password = ""
			return result
		}
	})
}


//Enroll user to a class

module.exports.enroll = async (data) => {


	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.enrollments.push({courseId: data.courseId});

		return user.save().then((user, error) => {

			if (error) {
				return false
			} else {
				return true
			}
		})
	})

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		course.enrollees.push({userId: data.userId});


		return course.save().then((course, error) => {


			if(error) {
				return false
			} else {
				return true
			}
		})
	})


	if(isUserUpdated && isCourseUpdated) {
		return true
	} else {
		return false;
	}

}